<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Image;
use AppBundle\Form\ImageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $images = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Image')
            ->findAll();

        return $this->render('@App/Basic/index.html.twig', array(
            'images' => $images,
        ));
    }

    /**
     * @Route("/user/{id}", requirements={"id": "\d+"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(int $id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);
        $images = $this->getUser()->getImages();

            return $this->render('@App/Basic/profile.html.twig', array(
                'user' => $user,
                'images' => $images,
            ));
    }


    /**
     * @Route("/image/new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function newAction(Request $request)
    {
        $image = new Image();

        $form = $this->createForm(ImageType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->getData();
            $image->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->flush();

            $user = $this->getUser();
            return $this->redirectToRoute("app_basic_profile", ['id' => $user->getId()]);
        }
        return $this->render('@App/Basic/image_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/image/{id}/remove", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeAction(int $id)
    {
        $image = $this->getDoctrine()
            ->getRepository('AppBundle:Image')
            ->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();

        $user = $this->getUser();

        return $this->redirectToRoute("app_basic_profile", ['id' => $user->getId()]);
    }
}
