<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Restaurant;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;


    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setName('Admin')
            ->setUsername('admin')
            ->setEmail('123@mail.ru')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN'])
            ;

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '123');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
